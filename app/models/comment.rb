class Comment < ActiveRecord::Base

	#Association
	belongs_to :post
	
	#Validation
	validates_presence_of :post_id
	validates_presence_of :body	
end
