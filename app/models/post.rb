class Post < ActiveRecord::Base

	#Association
	has_many :comments, dependent: :destroy
	
	#Validation
	validates_presence_of :title
	validates_presence_of :body
	
end
